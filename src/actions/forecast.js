import { getUrl } from '../services/urlConcatenator';

export const chosenCity = (value) => {
    return {
        type: 'CHOSEN_CITY',
        value
    }
};

export const updateForecast = (weatherForecast) => {
    console.log(weatherForecast)
    return {
        type: 'UPDATE_FORECAST',
        weatherForecast
    }
};

export const fetchForecast = (city) => {
    return async (dispatch) => {
        const url = getUrl('http://api.weatherstack.com/current?access_key=d094f47bb20878ba8b1fdfda3c3ebc4c&query=', city);
        const response = await fetch(url);
        const dataWeather = await response.json();
        dispatch(updateForecast(dataWeather));
        console.log(dataWeather)
        if (dataWeather != null) {
            let weatherForecast =
            {
                city: dataWeather.location.name,
                localtime: dataWeather.location.localtime,
                weather_description: dataWeather.current.weather_descriptions,
                temperature: dataWeather.current.temperature,
                weather_icon: dataWeather.current.weather_icons,
                wind_speed: dataWeather.current.wind_speed,
                humidity: dataWeather.current.humidity
            }
            dispatch(updateForecast(weatherForecast))
        }
    }
};
