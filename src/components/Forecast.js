import React from 'react';
import ChooseCityInput from './ChooseCityInput';
import PlaceDateForecast from './PlaceDateForecast';
import WeatherResult from './WeatherResult';
import SearchButton from './SearchButton';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { chosenCity, fetchForecast } from "../actions/forecast";


class Forecast extends React.Component{


    fetchForecast = () => {
        this.props.fetchForecast(this.props.city);
    }    

    render(){
        return (
            <div className="forecast">
                <PlaceDateForecast />
                <WeatherResult />
                <ChooseCityInput onChange={(event) => this.props.chosenCity(event.target.value)} value={this.props.city} />
                <SearchButton onClick={this.fetchForecast}/>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        city: state.forecastReducer.city,
        concatenatedUrl: state.forecastReducer.concatenatedUrl
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        chosenCity,
        fetchForecast
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Forecast)


